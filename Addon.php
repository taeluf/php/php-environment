<?php

namespace Env;

/**
 * a class for convenience methods related to environment
 */
class Addon {

    /**
     * Load mysql pdo settings from json env file & return a pdo instance
     *
     * Your json should include the following entries `mysql.host`, `mysql.dbname`, `mysql.user`, and `mysql.password`
     *
     * @param $env_file the absolute path to a file containing your settings
     * @return a pdo instance
     */
    static public function get_pdo_mysql(string $env_file): \PDO {
        $settings = json_decode(file_get_contents($env_file),true);
        $pdo = new \PDO('mysql:dbname='.$settings['mysql.dbname'].';host='.$settings['mysql.host'],
            $settings['mysql.user'],$settings['mysql.password']);
        return $pdo;
    }
    
    /**
     * Load mysql pdo settings from json env file & return a pdo instance. use mysql2.dbname, but same login settings?
     *
     * EXPECT THIS FUNCTION TO BREAK
     *
     * Your json should include the following entries `mysql.host`, `mysql.dbname`, `mysql.user`, and `mysql.password`
     *
     * @param $env_file the absolute path to a file containing your settings
     * @return a pdo instance
     */
    static public function get_pdo_mysql2(string $env_file): \PDO {
        $settings = json_decode(file_get_contents($env_file),true);
        $pdo = new \PDO('mysql:dbname='.$settings['mysql2.dbname'].';host='.$settings['mysql.host'],
            $settings['mysql.user'],$settings['mysql.password']);
        return $pdo;
    }
}


