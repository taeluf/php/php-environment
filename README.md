# PHP Environment
Example usage:
```php

<?php
//load the class
require(__DIR__.'/Env.php');

$forceShowErrors = false;
$forceDisableErrors = false;
$env = new Env();
$env->is(['host.local',$forceShowErrors],[!$forceDisableErrors])->showErrors();
$env->load(__DIR__.'/secret.json');
$env->is(['host.local'])->require($env->get('autoload.local'));
$env->isnt(['host.local'])->require(__DIR__.'/vendor/autoload.php');


RDB::setup('mysql:host='.$env->get('mysql.host').';dbname='.$env->get('mysql.dbname'), 
    $env->get('mysql.user'), $env->get('mysql.password')
);


$liaison = new \Liaison();
$primary = new \Lia\Package($liaison,__DIR__.'/main',['name'=>'Primary']);


$env->is(['host.local'])->do(function($lia){
    $draft = new \Lia\Package($lia,__DIR__.'/draft',['name'=>'Draft']);
}, $liaison);

//liaison stuff
$liaison->set('lia.cacheDir', $primary->dir('cache'));
$liaison->set('lia.resource.useCache',false);

$liaison->deliver();
```

## Other Example
Get a PDO instance
```php
<?php
$pdo = \Env\Addon::get_pdo_mysql('/absolute/path/to/your/env.json'); 
```


