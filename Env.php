<?php

/**
* Run different code based upon the environment you're in
*
*/
class Env {
    
    protected $vals = [];
    /** EnvHole instance */
    public $hole;
    
    public function __construct(){
        $this->hole = new EnvHole();
        $hostType = $this->hostType();
        $this->set('host.local',$hostType=='local');
        $this->set('host.remote',$hostType=='prod'||$hostType=='stage');
        $this->set('host.prod',$hostType=='prod');
        $this->set('host.stage',$hostType=='stage');
        
    }
    
    /**
    *
    * @return local, stage, or prod
    */
    public function hostType(){
        $host = $_SERVER['HTTP_HOST'];
        $parts = explode('.',$host);
        $tld = array_pop($parts);
        $tldParts = explode(':', $tld);
        $tld = $tldParts[0];
        $sub = array_shift($parts);
        if ($tld=='localhost')return 'local';
        else if ($sub=='stage'||$sub=='staging')return 'stage';
        
        return 'prod';
    }
    
    public function isnt($orParams, ...$andParams){
        $bool = $this->is_bool($orParams, ...$andParams);
        return $bool ? $this->fail() : $this->success();
    }
    public function is($orParams , ...$andParams){
        $bool = $this->is_bool($orParams, ...$andParams);
        return $bool ? $this->success() : $this->fail();
    }
    
    public function is_bool($orParams, ...$andParams){
        $andParams = [$orParams, ...$andParams];
        foreach ($andParams as $i => $and){
            $orPasses = false;
            foreach ($and as $i => $or){
                if ($or===false)continue;
                else if ($or===true){
                    $orPasses = true;
                    break;
                }
                if (!is_string($or)){
                    throw new \Exception("A non-bool non-string key value was passed...");
                }
                $val = $this->get($or);
                if ($val===true){
                    $orPasses = true;
                    break;
                }
            }
            if (!$orPasses)return false;
            
        }
        return true;
    }
    public function do($function, ...$args){
        return $function(...$args);
    }

    public function has($key){
        return isset($this->vals[$key]);
    }
    public function get($key){
        return $this->vals[$key] ?? null;
    }
    public function set($key,$value){
        $this->vals[$key] = $value;
    }
    
    public function fail(){
        return $this->hole;
    }
    public function success(){
        return $this;
    }
    
    
    public function showErrors(){
        ini_set('display_errors',1);
        error_reporting(E_ALL);
    }
    public function load($jsonFile){
        $content = file_get_contents($jsonFile);
        $data = json_decode($content,true);
        foreach ($data as $key=>$value){
            $this->set($key,$value);
        }
    }
    public function require($phpFile,$once=true){
        $once ? require_once($phpFile) : require($phpFile);
    }
    
    public function asBool(){
        return true;
    }
}

class EnvHole {
    public function __call($method,$args){
        return $this;
    }
    public function asBool(){
        return false;
    }
}
